<?php
get_header();
pageHeader();
while (have_posts()){
    the_post(); ?>




    <!-- START MAIN SECTION -->
    <main class="main">
        <div class="main_wrapper">
            <!-- START ARTICLE SECTION -->
            <section class="main_articles">
                <article class="main_article">
                    <h2><?php the_title(); ?></h2>
                    <div class="main_article_detail">
                        <span> نویسنده: <?php the_author_posts_link(); ?> </span>
                        <span>سشنبه 15 فروردین 1399</span>
                    </div>
                    <div class="main_article_content">
                            <?php the_content() ?>
                        <div class="main_article_footer">
                            <span>  دسته بندی:  <?php echo get_the_category_list() ?></span>
                            <span> برچسب ها:<?php echo get_the_tag_list()  ?>  </span>

                        </div>

                    </div>
                </article>
            </section>
        </div>

        <!-- START FOOTER SECTION -->
        <footer class="footer">
            <p>طراحی شده با ❤️ توسط <a href="">خودم</a></p>
        </footer>
        <!-- END FOOTER SECTION -->
    </main>

    <!-- END MAIN SECTION -->


<?php }
get_footer();
?>
