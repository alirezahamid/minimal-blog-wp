<?php
function blog_files (){
    wp_enqueue_style('blog_main_style',get_stylesheet_uri(),NULL , microtime());
    wp_enqueue_script('blog_main_script', get_theme_file_uri('/assets/scripts/main.js'));
}
add_action('wp_enqueue_scripts','blog_files');

function blog_features(){
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    register_nav_menu('headerMenuLocation','Header Menu Loacation');
}
add_action('after_setup_theme','blog_features');

function pageHeader($args = NULL)
{

    if(!$args['image']){ ?>
        <header class="header">
            <div class="header_image" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)"</div>
        </header>
    <?php } elseif ($args['image']){ ?>
        <header class="header">
            <div class="header_image" style="background-image: url(<?php echo $args['image'] ?>)"</div>
        </header>
   <?php } ?>


<?php } ?>
