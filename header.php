<!DOCTYPE html>
<html <?php language_attributes(); ?> dir="rtl">

<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>

<body <?php body_class();?>>
<div class="site_wrapper">
    <!-- START NAV -->
    <nav class="navBar">
        <div class="container">
            <div class="navBar_inner_container">
                <i class="navBar_menuButton" id="navBar_menuButton" data-eva="menu-outline"></i>
                <div class="navBar_headline">
                    <a href="<?php echo site_url(); ?>"><h1>وبلاگ علیرضا حمید</h1></a>
                </div>
                <menu class="navBar_menu">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'headerMenuLocation'
                    )) ?>
<!--                    <ul>-->
<!--                        <li><a href="index.html">خانه</a></li>-->
<!--                        <li><a href="about.html">درباره من</a></li>-->
<!--                    </ul>-->
                </menu>
            </div>
        </div>


    </nav>
    <!-- Mobile Menu -->

    <div class="menuBar">
        <div class="container">
            <div class="menuBar_inner_container">
                <ul>
                    <li><a href="index.html">خانه</a></li>
                    <li><a href="about.html">درباره من</a></li>
                </ul>

            </div>

        </div>
    </div>
    <!-- END NAV -->


    <!-- START HEADER SECTION -->

    <!-- END HEADER SECTION -->
