<?php get_header();
      pageHeader(array(
        'image' => get_theme_file_uri('/assets/images/codeCover.jpg')
        ))
?>

    <!-- START MAIN SECTION -->
    <main class="main archive">
        <div class="main_wrapper">
            <!-- START INTRO SECTION -->
            <section class="intro">
                <div class="intro_content">
                    <h2><?php the_archive_title() ?></h2>
                    <p><?php the_archive_description(); ?></p>
                </div>
            </section>
            <!-- START ARTICLE SECTION -->
            <section class="main_articles">
                <?php
                    while (have_posts()){
                        the_post();
                ?>
                <article class="main_article">
                    <h2><a href="<?php the_permalink(); ?>" target="_balnk"> <?php the_title(); ?>  </a></h2>
                    <div class="main_article_detail">
                        <span>نویسنده: <?php the_author_posts_link(); ?> </span>
                        <span>سشنبه 15 فروردین 1399</span>
                    </div>
                    <p>
                        <?php the_excerpt();
                         // or the_field('desc')
                        ?>
                    </p>

                    <div class="main_article_footer">
                        <span>  دسته بندی:  <?php echo get_the_category_list() ?></span>
                        <span> برچسب ها:<?php echo get_the_tag_list()  ?>  </span>
                    </div>
                </article>
                <?php } ?>
            </section>
        </div>

        <!-- START FOOTER SECTION -->
        <footer class="footer">
            <p>طراحی شده با ❤️ توسط <a href="">خودم</a></p>
        </footer>
        <!-- END FOOTER SECTION -->
    </main>

    <!-- END MAIN SECTION -->
<?php get_footer(); ?>
