<?php get_header();
      pageHeader(array(
        'image' => get_theme_file_uri('/assets/images/codeCover.jpg')
        ))
?>

    <!-- START MAIN SECTION -->
    <main class="main">
        <div class="main_wrapper">
            <!-- START INTRO SECTION -->
            <section class="intro">
                <div class="intro_content">
                    <h2>معرفی وبلاگ خودم</h2>

                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                        چاپگرها
                        و
                        متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد
                        نیاز و
                        کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته،
                        حال
                        و
                        آینده
                        شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه
                        ای
                        علی
                        الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که
                        تمام
                        و
                        دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی
                        دستاوردهای
                        اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>
                    <div class="intro_social">
                        <a href=""><span>Github</span> <i data-eva="github-outline" data-eva-width="20"
                                                          data-eva-fill="#625757"></i></a>
                        <a href=""><span>Linkedin</span> <i data-eva="linkedin-outline" data-eva-width="20"
                                                            data-eva-fill="#625757"> </i></a>
                        <a href=""><span>alireza@alireza.today</span> <i data-eva="at-outline" data-eva-width="20"
                                                                         data-eva-fill="#625757"> </i></a>
                    </div>
                </div>
            </section>
            <!-- START ARTICLE SECTION -->
            <section class="main_articles">
                <?php
                    while (have_posts()){
                        the_post();
                ?>
                <article class="main_article">
                    <h2><a href="<?php the_permalink(); ?>" target="_balnk"> <?php the_title(); ?>  </a></h2>
                    <div class="main_article_detail">
                        <span>نویسنده: <?php the_author_posts_link(); ?> </span>
                        <span>سشنبه 15 فروردین 1399</span>
                    </div>
                    <p>
                        <?php the_excerpt();
                         // or the_field('desc')
                        ?>
                    </p>

                    <div class="main_article_footer">
                        <span>  دسته بندی:  <?php echo get_the_category_list() ?></span>
                        <span> برچسب ها:<?php echo get_the_tag_list()  ?>  </span>
                    </div>
                </article>
                <?php } ?>
            </section>
        </div>

        <!-- START FOOTER SECTION -->
        <footer class="footer">
            <p>طراحی شده با ❤️ توسط <a href="">خودم</a></p>
        </footer>
        <!-- END FOOTER SECTION -->
    </main>

    <!-- END MAIN SECTION -->
<?php get_footer(); ?>
